# Trabalho de redes 2017 segundo semestre
* Ariel Palmeira 7971834
* Henrique Hissao Saggese Okomura 8921155
* Guilherme Oliveira 8921711

## API de requisições:
As requisições são todas no formato JSON

### AUTENTICAR
Faz uma requisição ao banco com o número do cartão e sua senha para atenticar a secção
#### Formato da requisição:
```JSON
{
  "operacao": "AUTENTICAR",
  "cartao": "<número do cartao>",
  "senha": "<quantia sacada>"
}
```
#### Formato da resposta:
```JSON
{
  "status": "<status da requisição, OK ou ERROR>",
  "mensagem": ""
}
```

### SAQUE
Faz uma requisição ao banco para sacar dinheiro no caixa eletrônico
#### Formato da requisição:
```JSON
{
  "operacao": "SAQUE",
  "conta": "<número da conta autenticada>",
  "valor": "<quantia sacada>"
}
```
#### Formato da resposta:
```JSON
{
  "status": "<status da requisição, OK ou ERROR>",
  "mensagem": ""
}
```

### DEPOSITO
faz uma requisição ao banco para depositar dinheiro no caixa eletrônico
#### Formato da requisição:
```JSON
{
  "operacao": "DEPOSITO",
  "conta": "<número da conta autenticada>",
  "valor": "<quantia depositada>"
}
```
#### Formato da resposta:
```JSON
{
  "status": "<status da requisição, OK ou ERROR>",
  "mensagem": "Seu saldo passou de <balanço anterior> para <novo balanço>"
}
```

### SALDO
Faz uma requisição ao banco pelo saldo de uma conta
#### Formato da requisição:
```JSON
{
  "operacao": "SALDO",
  "conta": "<número da conta autenticada>"
}
```
#### Formato da resposta:
```JSON
{
  "status": "<status da requisição, OK ou ERROR>",
  "mensagem": "Seu saldo é <balance>"
}
```

### TRANSFERENCIA
Faz uma requisição ao banco para realizar uma transferencia de uma conta à outra
#### Formato da requisição:
```JSON
{
  "operacao": "TRANSFERENCIA",
  "conta": "<número da conta autenticada>",
  "valor": "<quantia sacada em reais>",
  "destinatario": "<número da conta>"
}
```
#### Formato da resposta:
```JSON
{
  "status": "<status da requisição, OK ou ERROR>",
  "mensagem": ""
}
```
