- Descrição da aplicação (pode ser curto)

- Como executar

- Compilar (Não se aplica)

- Como usar

- Especificação do protocolo de aplicação
  - Tipos de mensagens
  - Formato de mensagem
  - Qual protocolo da camada de transporte está sendo usado (TCP/UDP)
  - Justificativas

- Testes que foram feitos
  - Entradas e saídas

- Problemas

- Fontes
