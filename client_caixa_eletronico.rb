require 'socket'
require 'json'

puts 'Configurando o sistema do caixa eletrônico...'

if ARGV[0]
  SERVER_ADDRESS = ARGV[0]
else
  puts 'Digite o endereço IP do servidor:'
  SERVER_ADDRESS = gets.chomp
end

if ARGV[1]
  SERVER_PORT = ARGV[1].to_i
else
  puts 'Digite a PORTA do servidor:'
  SERVER_PORT = gets.chomp.to_i
end

def gets
  STDIN.gets
end

def clear
  system "clear" or system "cls"
end

def bank_request(request, port = SERVER_PORT)
  begin
    socket = TCPSocket.open(SERVER_ADDRESS, port)

    socket.puts(request.to_json)

    response = socket.gets

    socket.close

    JSON.parse(response)
  rescue SocketError
    puts 'HOST e/ou PORT não encontradas!'
    puts 'usage e.g.: ruby client_caixa_eletronico.rb localhost 3000'
  end
end

def authenticate
  not_authenticated = true
  account = ''
  while not_authenticated do
    request = {}

    puts 'Por favor insira o cartão(digite o número do mesmo):'
    request[:conta] = gets.chomp

    puts 'Digite sua senha por favor:'
    request[:senha] = gets.chomp

    response = bank_request(request)

    not_authenticated = !response['autenticado']

    if not_authenticated
      clear
      puts 'Senha incorreta, Tente novamente!'
    else
      account = request[:conta]
    end
  end
  account
end

def get_positive_float
  invalid_value = true
  value = ''
  while invalid_value
    begin
      value = Float(gets.chomp)
      raise if value <= 0
      invalid_value = false
    rescue
      puts 'Valor inválido! Tente novamente'
    end
  end
  value
end

loop do
  clear
  puts 'Bem vindo ao caixa eletrônico!'
  request = { conta: authenticate }

  puts 'O que deseja fazer?'
  puts '* SAQUE'
  puts '* DEPOSITO'
  puts '* SALDO'
  puts '* TRANSFERENCIA'
  request[:operacao] = gets.chomp.upcase

  case request[:operacao]
  when 'SAQUE'
    puts 'Quanto?'
    request[:valor] = get_positive_float
  when 'DEPOSITO'
    puts 'Quanto?'
    request[:valor] = get_positive_float
  when 'TRANSFERENCIA'
    puts 'Quanto?'
    request[:valor] = get_positive_float
    puts 'Para quem?'
    request[:destinatario] = gets.chomp
  else
    puts 'Operação desconhecida!'
  end

  resposta = bank_request(request, (1 + SERVER_PORT))


  puts 'Operação realizada com sucesso!!' if resposta['status'] == 'OK'
  puts resposta['mensagem']
  puts 'Pressione enter para prosseguir'
  gets
end
