require 'socket'
require 'json'

ACCOUNTS_INFO = {
  '123' => {
    'password' => 'qwe',
    'balance' => 42
  },
  '456' => {
    'password' => 'asd',
    'balance' => 8000.01
  },
  '789' => {
    'password' => 'zxc',
    'balance' => 9000.01
  }
}

# Check if you passed an argument,
# method_nameand if you did checks if when it is parsed to integer it is
# more than 0, if so sets the server's PORT as the argument,
# otherwise the PORT defaults to 3000
PORT = !ARGV[0].nil? && ARGV[0].to_i > 0 ? ARGV[0].to_i : 3000

authentication_socket = TCPServer.new PORT
operations_socket = TCPServer.new(PORT + 1)
puts "Server listening on PORT #{PORT}"

def authenticated?(request)
  password = ACCOUNTS_INFO.dig(request['conta'], 'password')
  (password || false) && password == request['senha']
end

def get_balance_for(account)
  ACCOUNTS_INFO.dig(account, 'balance')
end

def withdrawal value, from:
  balance = get_balance_for(from)
  if balance.nil?
    { success: false, message: 'Saque inválido' }
  elsif balance < value
    { success: false, message: 'Você não tem saldo suficiente' }
  else
    ACCOUNTS_INFO[from]['balance'] -= value
    { success: true , message: "Seu saldo passou de #{balance} para #{balance - value}"}
  end
end

def deposit value, receiver:
  balance = get_balance_for(receiver)
  unless balance.nil?
    ACCOUNTS_INFO[receiver]['balance'] += value
  end
end

def transfer value, from:, to:
  receiver_balance = get_balance_for(to)
  emitter_balance = get_balance_for(from)
  if receiver_balance.nil?
    { success: false, message: 'Destinatário inválido' }
  elsif emitter_balance.nil?
    { success: false, message: 'Sacado inválido' }
  elsif ((emitter_balance - value) <= 0)
    { success: false, message: "Você não tem saldo para fazer uma transferência de #{value}" }
  else
    ACCOUNTS_INFO[from]['balance'] -= value
    ACCOUNTS_INFO[to]['balance'] += value
    { success: true }
  end
end

authentication_thread = Thread.start do
  loop do
    Thread.start(authentication_socket.accept) do |client|
      request = JSON.parse(client.gets)
      client.puts({ autenticado: authenticated?(request) }.to_json)
      client.close
    end
  end
end

operations_thread = Thread.start do
  loop do
    Thread.start(operations_socket.accept) do |client, main|
      request = JSON.parse(client.gets)
      response = {}
      case request['operacao']
      when 'SALDO'
        balance = get_balance_for(request['conta'])
        response['mensagem'] = "Seu saldo é #{balance}"
        response['status'] = 'OK'
      when 'SAQUE'
        withdrawal_result = withdrawal(request['valor'], from: request['conta'])
        response['mensagem'] = withdrawal_result[:message]
        if withdrawal_result[:success]
          response['status'] = 'OK'
        else
          response['status'] = 'ERROR'
        end
      when 'DEPOSITO'
        previous_balance = get_balance_for(request['conta'])
        new_balance = deposit(request['valor'], receiver: request['conta'])
        response['mensagem'] = "Seu saldo passou de #{previous_balance} para #{new_balance}"
        response['status'] = 'OK'
      when 'TRANSFERENCIA'
        transfer_result = transfer(request['valor'], from: request['conta'], to: request['destinatario'])
        if transfer_result[:success]
          response['status'] = 'OK'
        else
          response['mensagem'] = transfer_result[:message]
          response['status'] = 'ERROR'
        end
      else
        response['status'] = 'ERROR'
      end
      client.puts(response.to_json)
      client.close
    end
  end
end

authentication_thread.join
puts 'Thread de autenticação terminou'
operations_thread.join
puts 'Thread de operações terminou'
